#!/usr/bin/env python
# -*- coding: utf8 -*-
import os,sys 


 
basedir = os.path.abspath(os.path.dirname(__file__))

CSRF_ENABLED = True
SECRET_KEY = 'you-will-never-guess'

SQLALCHEMY_DATABASE_URI = 'mysql://root:root@localhost/app'
SQLALCHEMY_ECHO=True
#SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
WHOOSH_BASE = os.path.join(basedir, 'search.db')



ADMINS=['warriorzcx@126.com']

# email server
MAIL_SERVER = 'your.mailserver.com'
MAIL_PORT = 25
MAIL_USE_TLS = False
MAIL_USE_SSL = False
MAIL_USERNAME = 'you'
MAIL_PASSWORD = 'your-password'


# pagination
POSTS_PER_PAGE = 3
max_search_results = 50

UPLOAD_FOLDER = 'statusupimg'