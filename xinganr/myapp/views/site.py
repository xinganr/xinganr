#!/usr/bin/env python
#-*-coding:utf-8-*-
from flask.ext.login import current_user
from flask import Blueprint, render_template,g
from myapp.forms.users import StatusForm
from config import POSTS_PER_PAGE
from myapp import db

import Image,ImageDraw,ImageFont
import random
import math, string  

siteapp=Blueprint("site", __name__,static_folder='static',template_folder='templates')

'''
@siteapp.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated():

        db.session.add(g.user)
        db.session.commit()
        
'''


@siteapp.route('/',methods=['GET'])

def index():
	return render_template('index.html')

