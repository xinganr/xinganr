#!/usr/bin/env python
#-*-coding:utf-8-*-
from flask import Blueprint, render_template, g
from myapp.models import *

pictureapp=Blueprint("picture",__name__,static_folder='static',template_folder='templates')

@pictureapp.route('/')
def index():
	#g.posts = Post.objects.all()
	return render_template('pictures.html')