#!/usr/bin/env python
#-*-coding:utf-8-*-
from flask import Blueprint, render_template, g
from myapp.models import *

diaryapp=Blueprint("diary",__name__,url_prefix="/diary")

@diaryapp.route('/')
def index():
	g.posts = Post.objects.all()
	return render_template('homepage.html')