#!/usr/bin/env python
#-*-coding:utf-8-*-
import logging
logger=logging.getLogger(__name__)

from flask import Flask,Blueprint,request,session,g,url_for,redirect,json,jsonify,abort
from flask import render_template,flash
from myapp.extensions import oid,oau,babel,pag
from flask import current_app as app 
from flask.ext import login
from flask.ext import wtf
from flask.ext.login import LoginManager,login_user,logout_user,current_user,login_required
from myapp.forms.users import *
#from config import posts_per_page,max_search_results
from myapp.utils.filters import *
#from myapp.utils.user import auth_user
from myapp.extensions import login_manager
from myapp import db
from datetime import datetime

from PIL import Image,ImageDraw,ImageFont
import sys,os

from myapp.models import User,Status,Log#,UserInfo,ROLE_USER,ROLE_ADMIN,Childinfo,Log,Comment,Picture,Category)
userapp=Blueprint("user",__name__,static_folder='static',template_folder='templates')

from werkzeug.utils import secure_filename


# Set this configuration to absolute or relative path to upload directory.



# statusupimg_path=''
# imgslist=[]

# @userapp.route('/save_statusupimg',methods=['GET','POST'])
# @login_required
# def save_statusupimg(filestorage, app=app):
#     "Save a Werkzeug file storage object to the upload folder."
#     global statusupimg_path
#     statusupimg_name = secure_filename(filestorage.filename)
#     statusupimg_path = os.path.join(app.config['UPLOAD_FOLDER'], statusupimg_name)
#     filestorage.save(statusupimg_path)
    

# @userapp.route('/save_statusupimgs',methods=['GET','POST'])
# @login_required
# def save_statusupimgs(request=request, app=app):
#     global imgslist
#     "Save all files in a request to the app's upload folder."
#     for _, filestorage in request.files.iteritems():
#         # Workaround: larger uploads cause a dummy file named '<fdopen>'.
#         # See the Flask mailing list for more information.
#         if filestorage.filename not in (None, 'fdopen', '<fdopen>'):
#             save_statusupimg(filestorage, app=app)
#             global statusupimg_path
#             print 'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy' 
#             print statusupimg_path
#             imgslist.append(statusupimg_path)
#             print imgslist
#             print len(imgslist)
#     for x in len(imgslist):
#         imgsitem=jsonify(x=imgslist[x-1])
        
def save_file(filestorage, app=app):
    "Save a Werkzeug file storage object to the upload folder."
    filename = secure_filename(filestorage.filename)
    filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    filestorage.save(filepath)


def save_files(request=request, app=app):
    "Save all files in a request to the app's upload folder."
    for _, filestorage in request.files.iteritems():
        # Workaround: larger uploads cause a dummy file named '<fdopen>'.
        # See the Flask mailing list for more information.
        if filestorage.filename not in (None, 'fdopen', '<fdopen>'):
            save_file(filestorage, app=app)


    



class Anonymous(login.AnonymousUserMixin):
	user=User(email='',nickname=u'游客',password='',headimg='')

class LoginUser(login.UserMixin):
	"""Wraps User object  for Flask-Login"""
	def __init__(self,user):
	    self.id=user.id
	    self.user=user
            self.headimg=user.headimg

            #self.avatar=user.avatar(size)
            self.nickname=user.nickname

login_manager.login_view = 'user.login'
login_manager.login_message = u'需要登陆后才能访问本页'

#login_manager.anonymous_user=Anonymous


@login_manager.user_loader
def load_user(users_id):
    user=User.query.get(users_id)
    return user and LoginUser(user) or None
#def login_user(user, remember=False):
    """ 登陆用户并更新最近登陆时间 """
#    login.login_user(LoginUser(user),remember=False)
    #user.login_time = datetime.now()
'''
@userapp.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated():
        #g.user.last_seen = datetime.now()
        db.session.add(g.user)
        db.session.commit()
        #g.search_form = SearchForm()
'''
 

#用户注册
@userapp.route('/signup',methods=['GET','POST'])
def signup():
	error=None
	next_url = request.args.get('next', url_for('site.index'))
	form = SignupForm(request.form,csrf_enabled=False)

        print 'bbbbbbbbbbbbbbbbbbbbbbbbbbbb!!!!!!!!!!!!!'
        print form.headimg

	#if current_user.is_authenticated():
	if current_user.is_authenticated():
		return redirect(url_for('user.userprofile'))

		#app.logger.info('request.form: ' + repr(request.values))
	
	#app.logger.info('>>> Signup user: ' + repr(dict(form.data, password='<MASK>',confirmpassword='<MASK>')))
    
            
	if form.validate_on_submit():
            f=request.files['headimg']
            f.save('/home/Alex/myproject/envflask/xinganr/myapp/static/headicon/%s' %form.user.nickname)
            fpath=('/static/headicon/%s' %form.user.nickname)
            user=User(email=form.email.data,nickname=form.nickname.data,password=form.password.data,headimg=fpath)
            db.session.add(user)
            db.session.commit()
	    flash(u'恭喜您！注册成功')
	    return redirect(url_for('user.login'))
        else:
            return render_template('signup.html',title=u'欢迎来到心肝儿网!',form=form)
'''

	if request.method=='POST':
		nickname=request.form.get('nickname').lower()
		email=request.form.get('email').lower()
		password=request.form.get('password')
		confirm_password=request.form.get('confirm_password')

		if not all([nickname,email,password,confirm_password]):
			error=u'请填满所有信息'
		elif password != confirm_password:
			error=u'密码不一致！请重新确认密码'
		elif User.query.filter_by(username=username).first:
			error=u'该用户名已被注册!'
		elif User.query.filter_by(email=email).first():
			error=u'该邮箱已被注册!'
		else:
			create_user(username,email,password)
			flash(u'恭喜您！注册成功')
			return redirect(url_for('user.login'))
        return render_template('signup.html',title=u'欢迎来到心肝儿网!',form=form,error=error)
'''


	
#用户登录
@userapp.route('/login',methods=['GET','POST'])
def login():
    error=None
    #user = User.query.filter_by(email = email).first()
    form = LoginForm(request.form, csrf_enabled=False)
    #user=form.user
    #form=LoginForm(request.form, csrf_enabled=False)
	#next_url=request.args.get('next',url_for('site.index'))
    
    #print form.validate_on_submit
    #if form.validate_on_submit():
    #form = LoginForm(request.form)
    #g.user=form.user

    #print form.validate()
    #print form.is_submitted()
    #print form.validate_on_submit()
    print form.errors
    #if g.user is not None and g.user.is_authenticated():
    #   return redirect(url_for('site.index'))
    
    if form.validate_on_submit():
        #session['remember_me'] = form.remember_me.data
        #user=User.query.get(users_email).first()
        login_user(form.user,remember=False)

        
        print form.user.is_active()
        print form.user.is_anonymous()
        
        flash(u'登录成功！')
        return redirect(url_for('site.index'))

    return render_template('login.html', title = u'欢迎来到心肝儿网!',form = form)
 
    def after_login(resp):
	#user=User.query.filter_by(email=resp.email).first():
	nickname=User.make_valid_nickname(nickname)
	nickname=User.make_unique_nickname(nickname)
	user=User(nickname=resp.nickname,email=resp.email)
        db.session.add(user)
        db.session.commit()
        login_user(user,remember=False)

    
   

'''
        flash("Success")
        return redirect(url_for("user.index"))
    return render_template("login.html", form=form)
'''
@userapp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('site.index'))

@userapp.route('/userprofile')		
@userapp.route('/userprofile/<nickname>')
@login_required
#@login_required
def userprofile(nickname=None):
    #user = User.query.filter_by(nickname=nickname).first()
    g.user=current_user
    print g.user.headimg
    return render_template('homepage.html')


@userapp.route('/account_edit',methods=['GET','POST'])
@login_required
def account_edit():
    error=None
    g.user=current_user
    form=AccountEditForm(request.form,csrf_enabled=False)
   #print form.FileField.has_file()

    print form.validate_on_submit()
    print form.errors
    if form.validate_on_submit():
        #filename=secure_filename(form.headimg.data.filename)
        headimages=request.files['headimg']
        db.session.add(headimg)
        db.session.commit()
        #form.populate_obj(g.user.headimg)
        return render_template('homepage.html')
    else:
        #filename=None
        return render_template('account_edit.html')
        
    return render_template('homepage.html',form=form)


@userapp.route('/status',methods=['GET','POST'])
@login_required
def status():
    error=None

    statuses=Status.query

    g.user=current_user
    ggg=g.user.nickname
    print ggg
    form=StatusForm(request.form,csrf_enabled=False)
    #if request.method=='POST':
        #f=request.files['body']
        #u=form.user.nickname
        #f.save('/home/Alex/myproject/envflask/xinganr/myapp/static/statusimg/%s' %g.user.nickname)
            
 

    #print form.errors
    print form.is_submitted()
    print form.validate_on_submit()
    print 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!!!!!!!!!!!!!!!'
    print form.errors
    #return render_template('status.html')
    


    if form.validate_on_submit():

        f=request.files['statusupimg']
        if f:
            f.save('/home/Alex/myproject/envflask/xinganr/myapp/static/pic/status/%s' %form.body.data[0:5])
            fpath=('/static/pic/status/%s' %form.body.data[0:5])
            status = Status(user_id=g.user.id,body=form.body.data,statusupimg=fpath,time=datetime.now())
        else:
            status = Status(user_id=g.user.id,body=form.body.data,time=datetime.now())
        
        print datetime.now()
        print '--------------------'
        print status.time
        db.session.add(status)
        db.session.commit()
	flash(u'恭喜您！成功')
        #session['remember_me'] = form.remember_me.data
        return redirect(url_for('user.news'))
        #statuses=status.paginate(1,3,False)   
    statuses=statuses.order_by(Status.time.desc()).paginate(1,8,False)
    
    return render_template('news.html', title = u'欢迎来到心肝儿网!---'+ggg,statuses=statuses)


@userapp.route('/xinmail',methods=['GET','POST'])
@login_required
def xinmail():
    error=None
    
    return render_template('xinmail.html',title=u'我的心邮')


@userapp.route('/ckeditorupload',methods=['GET','POST'])

def ckeditorupload():
    g.user=current_user
    
    form=CkedituploadForm(request.form,csrf_enabled=False)

   

    if form.validate_on_submit():
        f=request.files['editor1']
        f.save('/home/Alex/myproject/envflask/xinganr/myapp/static/ckeditorupload/%s' %g.user.nickname)
        fpath=('/static/ckeditorupload/%s' %g.user.nickname)
        log = Log(upload=fpath,user_id=g.user.id)
       
        db.session.add(log)
        db.session.commit()

   

    return render_template('log_edit.html')


@userapp.route('/log',methods=['GET','POST'])
@login_required
def log():
    #f=request.files['editor1']
    #f.save('/home/Alex/myproject/envflask/xinganr/myapp/logs/%s' %g.user.nickname)
    #fpath=('/logs/%s' %g.user.nickname)
    #log=Log(body=fpath,user_id=g.user.id)
    return render_template('log.html')

@userapp.route('/logimg',methods=['POST'])
@login_required
def logimg():
    img=request.files['log_imgupload']
    img.save('//home/Alex/myproject/envflask/xinganr/myapp/static/logimg/%s' %g.user.nickname)
    imgpath=('/static/logimg/%s' %g.user.nickname)
    log_imgupload=request.json['log_imgupload']
    return {"result":log_imgupload}


@userapp.route('/get_current_user',methods=['GET','POST'])
@login_required
def get_current_user():
    g.user=current_user
    return jsonify(username=g.user.nickname)

@userapp.route('/create_headimg_thumbnail',methods=['GET','POST'])
@login_required
def create_headimg_thumbnail():
    g.user=current_user
    size=32,32
    path=('/home/Alex/myproject/envflask/xinganr/myapp/static/headicon/thumbnail/%s_thumbnail' %g.user.nickname)
    print os.path.isfile(path)
    if os.path.isfile(path):
        pass
    else:
        im = Image.open('/home/Alex/myproject/envflask/xinganr/myapp/static/headicon/%s' %g.user.nickname)
        im.thumbnail(size,Image.ANTIALIAS)
        im.save('/home/Alex/myproject/envflask/xinganr/myapp/static/headicon/thumbnail/%s_thumbnail' %g.user.nickname,"JPEG")
    return jsonify(headimg_thumb=('/static/headicon/thumbnail/%s_thumbnail' %g.user.nickname))






@userapp.route('/uploadify', methods=['GET', 'POST'])
def uploadify():
    if request.method == 'POST':
        save_files()
        return 'Uploaded'
    if request.method == 'GET':
        return render_template('news.html')

@userapp.route('/news',methods=['GET'])
@login_required
def news():
    error=None
    statuses=Status.query
    g.user=current_user
    ggg=g.user.nickname
    statuses=statuses.order_by(Status.time.desc()).paginate(1,8,False)
    return render_template('news.html', title = u'欢迎来到心肝儿网!---'+ggg,statuses=statuses)








