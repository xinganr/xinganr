#-*-coding:utf-8-*-
'''
from myapp.models import User
def auth_user(email, password):
    user = User.query.filter_by(email=email.lower()).first()
    if user is None:
        return None
    else:
        return user if user.check_password(password) else None

'''