#-*-coding:utf-8-*-

def thumbnail_filter(url, size='full', format='png'):
    parameters = urllib.urlencode({'url': url, 'type': format})
    api = 'http://api.snapito.com/web/abc123/%s?%s'
    return api % (size, parameters)


def shorter_url_filter(url):
    return pretty_url(re.sub(r'^https?://', '', url))


def pretty_url(url):
    return re.sub(r'(^https?://[^/\.]+\.[a-zA-Z]+)/$', r'\1', url)


def format_datetime_filter(value, format='%b %d, %Y'):
    return value.strftime(format)

def markdown_filter(text, safe_mode=None):
    if safe_mode == 'safe' or safe_mode == 'replace':
        safe_mode = 'replace'  # True
    elif safe_mode:
        safe_mode = 'escape'
    return markdown(text, safe_mode=safe_mode)

def current_link_filter(path, view_name, class_name='active', **values):
    if path == url_for(view_name, **values):
        return class_name
    else:
        return ''

def auth_user(email, password):
    user = User.query.filter_by(email=email.lower()).first()
    if user is None:
        return None
    else:
        return user if user.check_password(password) else None


def time_passed(value):
    now = datetime.now()
    past = now - value
    if past.days:
        return u'%s天前' % past.days
    mins = past.seconds / 60
    if mins < 60:
        return u'%s分钟前' % mins
    hours = mins / 60
    return u'%s小时前' % hours

def auth_user(email, password):
    user = User.query.filter_by(email=email.lower()).first()
    if user is None:
        return None
    else:
        return user if user.check_password(password) else None