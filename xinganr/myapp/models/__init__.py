#-*-coding:utf-8-*-
import random,hashlib
from flask import request,url_for
import flask.ext.whooshalchemy as whooshalchemy
from werkzeug import cached_property,security
from werkzeug.security import generate_password_hash,check_password_hash
from datetime import datetime
#from myapp import app
from myapp import db,app
from myapp.utils.func import md5
ROLE_USER = 0
ROLE_ADMIN = 1
#db = SQLAlchemy(app)

followers=db.Table('followers',
	db.Column('follower_id',db.Integer, db.ForeignKey('users.id')),	    #用户id
	db.Column('followed_id',db.Integer, db.ForeignKey('users.id')))		#好友id

likers=db.Table('liker',
	db.Column('user_id',db.Integer, db.ForeignKey('users.id')),	   
	db.Column('log_id',db.Integer, db.ForeignKey('log.id')))	

#
def get_user(email=None,password=None):
    user = None
    
    if email:
        user = User.query.filter_by(email=email).first()
    elif password:
    	user=User.query.filter_by(password=password).first
    
    #    user = User.query.filter_by(nickname=nickname).first()
    #elif users.id:
    #    user = User.query.filter_by(id=users_id).first()

    return user
class UserInfo(db.Model):
    """
    用户信息表
    """
    #__tablename__ = 'user_info'

    id = db.Column(db.Integer, primary_key=True)
    motoo = db.Column(db.String(255)) # 座右铭
    introduction = db.Column(db.Text) # 个人简介
    phone = db.Column(db.String(15), unique=True, nullable=True) # 手机号码
    phone_status = db.Column(db.Integer, nullable=True) # 手机可见度: 0-不公开 1-公开 2-向成员公开
    photo = db.Column(db.String(255), nullable=True) # 存一张照片，既然有线下的聚会的，总得认得人才行

    #user = db.relationship('User', backref='user_info', uselist=False) 
    #user_id=db.Column(db.Integer,db.ForeignKey('users.id'))

    
    def __repr__(self):
        return "<UserInfo (%s)>" % self.users.id

class UserOpenID(db.Model):
    """
    用户绑定OpenID的表
    一个用户可以对应多个OpenID
    """
    #__tablename__ = 'user_openids'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False) # openid关联的用户
    openid = db.Column(db.String(255), nullable=False, unique=True) # 记录的 openid, 不能重复
    provider = db.Column(db.String(50), nullable=False) # openid的提供商，比如 google 

#用户信息表单
class User(db.Model):
    __tablename__ = 'users'

    id=db.Column(db.Integer, primary_key=True)  			            #用户id
    email=db.Column(db.String(50),nullable=False,unique=True)			#登录名
    nickname=db.Column(db.String(20),nullable=False,unique=True)		#用户昵称
    password=db.Column(db.String(20))	                                    #密码
    #confirm_password=db.Column(db.String(20))			        		#确认密码
    #state=db.Column(db.Integer)					  	                    #用户状态
    #rgtime=db.Column(db.DateTime, default=datetime.utcnow)			    #注册时间
    #last_login_time=db.Column(db.DateTime)			    		        #最后登录时间
    #exp=db.Column(db.Integer)					 	                    #用户经验值
    #gold=db.Column(db.Integer)						                    #用户金币
    headimg=db.Column(db.String(255))					                #头像
    #sex=db.Column(db.Integer)						                    #性别1为男,0为女
    #birthday=db.Column(db.String(20))					                #生日
    #qq=db.Column(db.String(12))						                    #QQ号
    #weibo=db.Column(db.String(50))						                #新浪微薄
    #role=db.Column(db.Integer)

    #user_info_id = db.Column(db.Integer, db.ForeignKey('user_info.id'))
    #user_info=db.relationship('UserInfo')

    #followed = db.relationship('User',secondary=followers,
	#			primaryjoin=(followers.c.follower_id==id),
	#			secondaryjoin=(followers.c.followed_id==id),
	#			backref=db.backref('followers',lazy='dynamic'),
	#			lazy='dynamic')                                         #和朋友表进行关联    
    #childinfos= db.relationship('Childinfo',backref='users',lazy='dynamic')
    #logs=db.relationship('Log',backref='users',lazy='dynamic')
    #pictures=db.relationship('Picture',backref='users',lazy='dynamic')
    #categorys=db.relationship('Category',backref='users',lazy='dynamic')
    #comments=db.relationship('Comment',backref='users',lazy='dynamic') 
    statuses = db.relationship('Status',backref ='author',lazy='dynamic')
    
    def __init__(self, email,password,nickname,headimg):
        self.nickname = nickname
        self.email = email
        self.password=password
        self.headimg=headimg
        #self.set_password(password)
      
        
    def __repr__(self):
        return '<User %r>' % self.nickname



    def set_password(self, password):
    	#pwhash=generate_password_hash(password,method='pbkdf2:sha1',salt_length=8)
    	#return pwhash
    	self.password=md5(password)


    def check_password(self,password):
    	#return check_password_hash(pwhash,password)
    	return self.password==md5(password)
	
#	if not self.password:
#		return False
#		if '$' not in self.password:
#			return False
#			salt, hsh = self.password.split('$')
#			passwd = '%s%s%s' % (salt, raw, db.app.config['PASSWORD_SECRET'])
#			verify = hashlib.sha1(passwd).hexdigest()
#			return verify == hsh

    def is_authenticated(self):
		return True

    def is_active(self):
		return True

    def is_anonymous(self):
		return False
	
    def get_id(self):
		return unicode(self.id)
		
   
    
    def avatar(self, size):
	    return 'http://www.gravatar.com/avatar/' + md5(self.email) +'?d=mm&s=' + str(size) #未加md5




    @staticmethod
#是用户名唯一
    def make_unique_nickname(nickname):
		if User.query.filter_by(nickname=nickname):
			return nickname
			version=2
		while True:
			new_nickname=nickname+str(version)
			if User.query.filter_by(nickname=new_nickname).first()==None:
				break
			version+=1
		return new_nickname

#关注用户函数

    def follow(self,user):
		if not self.is_following(user):
			self.followed.append(user)
			return self
#取消关注用户
    def unfollow(self,user):
			if self.is_following(user):
				self.followed.remove(user)
				return self

#判断是否此user已经被关注

    def is_following(self,user):

		return self.followed.filter(followers.c.followed_id==
			users.id).count()>0

#返回所有关注用户的日志
    def followed_log(self):
		return Log.query.join(followers,followers.c.followed_id==
			Log.user_id).filter(followers.c.follower_id==
			self.id).order_by(Log.time.desc())

#喜欢日志
    def like(self,log):
		if not self.is_liking(log):
			self.liked.append(log)
			return self

#取消喜欢
    def unlike(self,log):
		if self.is_liking(log):
			self.liked.remove(log)
			return self

#判断用户是否已经关注此日志
    def is_liking(self,log):
		return self.liked.filter(likers.c.log_id==log.id).count()>0

#返回用户喜欢的日志
    def liked_log(self):
		return self.liked.order_by(Log.time.desc())
		
        #def show_posts(body):
        #	return Status.query.filter_by(body=body)

    #def __repr__(self):
	#	return '<User %r>' %self.email
	
class Userhead(db.Model):

	
	id=db.Column(db.Integer,primary_key=True)
	user_id=db.Column(db.Integer,db.ForeignKey('users.id'))	
	headimg=db.Column(db.String(255))

	def __repr__(self):
	    return '<Userhead %r>' %self.headimg

#孩子信息表单
class Childinfo(db.Model):
	__tablename__='child_infos'
	id=db.Column(db.Integer, primary_key=True)			        		#孩子id
	user_id=db.Column(db.Integer, db.ForeignKey('users.id'))				#用户id
	nickname=nickname=db.Column(db.String(20))				            #孩子昵称
	birthday=db.Column(db.String(20))					                #孩子生日
	sex=db.Column(db.String(1))					 	                    #孩子性别1为男，0为女
	star=db.Column(db.String(10))						                #孩子星座   
	#parent=db.relationship('User',backref=db.backref(
    #    'childinfos', lazy='dynamic'), lazy='select')

	#user=db.relationship('User',backref='child_infos', lazy='dynamic')

        def __repr__(self):
		return '<Childinfo %r>' %self.nickname
 
#发状态
class Status(db.Model):
	__searchable__=['body']	
	
	id=db.Column(db.Integer,primary_key=True)				 			#status id
	user_id=db.Column(db.Integer,db.ForeignKey('users.id'),nullable=False)				#用户id
	#category_id=db.Column(db.Integer,db.ForeignKey('category.id'))		#类型id
	
    
	body=db.Column(db.String(140))	
	statusupimg=db.Column(db.String(255))					    				
	time=db.Column(db.DateTime,default=datetime.now())					#发表时间
	mod_time=db.Column(db.DateTime)					 					#最后修改时间
	public=db.Column(db.Integer)						 				#是否公开
	public_time=db.Column(db.DateTime)					                #公开时间
	forwarded_num=db.Column(db.Integer)                                 #转载次数
	comment_count=db.Column(db.Integer,default=0)					  	#评论次数
	hot=db.Column(db.Integer)						 					#热度
    
	#author = db.relationship('User',backref = db.backref('status',lazy='dynamic') ,lazy='select')

        #categorys=db.relationship('Category',backref=db.backref('status',lazy='select'),lazy='select')
        #pictures=db.relationship('Picture',backref=db.backref('status',lazy='select'),lazy='select')
	#comments=db.relationship('Comment',backref=db.backref('status',lazy='select'),lazy='select')
	
    #    def show_status():
    #	    return Status.query.order_by(Status.time.desc())
	def __repr__(self):
	    return '<Status %r>' %self.body

#日志
class Log(db.Model):
	__searchable__=['body']	
	
	id=db.Column(db.Integer,primary_key=True)				 			#日志id
	user_id=db.Column(db.Integer,db.ForeignKey('users.id'))				#用户id
	category_id=db.Column(db.Integer,db.ForeignKey('category.id'))		#类型id
	
        upload=db.Column(db.String(255))
	body=db.Column(db.Text)						    				
	time=db.Column(db.DateTime,default=datetime.now())					#发表时间
	mod_time=db.Column(db.DateTime)					 					#最后修改时间
	public=db.Column(db.Integer,default=True)						 	#是否公开
	public_time=db.Column(db.DateTime,default=datetime.now())			#公开时间
	comment_count=db.Column(db.Integer,default=0)					  	#评论次数
	hot=db.Column(db.Integer)						 					#热度
	
	
	#author=db.relationship('User',backref=db.backref(
	#					'logs',lazy='select'),lazy='select')
	categorys=db.relationship('Category',backref=db.backref(
						'logs',lazy='select'),lazy='select')
	pictures=db.relationship('Picture',backref=db.backref(
						'log',lazy='select'),lazy='select')
	comments=db.relationship('Comment',backref=db.backref(
						'log',lazy='select'),lazy='select')

	#likers=db.relationship('User',secondary=likers,
	#					backref=db.backref('liked',
	#					lazy='dynamic'),lazy='dynamic')

	

	def __repr__(self):
		return '<Log %r>' %self.upload
	


#图片
class Picture(db.Model):
	id=db.Column(db.Integer,primary_key=True)							#图片id
	pic_address=db.Column(db.String(200))					            #图片路径
	log_id=db.Column(db.Integer,db.ForeignKey('log.id',ondelete='CASCADE'))		    #日志id
        status_id=db.Column(db.Integer,db.ForeignKey('status.id',ondelete='CASCADE'))
	def __repr__(self):
		return '<Picture %r>' %self.pic_address

#类型
class Category(db.Model):
	id=db.Column(db.Integer,primary_key=True)							#类型id
	category_name=db.Column(db.String(20))								#类型名
	
	def __repr__(self):
		return '<Picture %r>' %self.category_name

#评论
class Comment(db.Model):
	comment_id=db.Column(db.Integer,primary_key=True)					 #评论id
	log_id=db.Column(db.Integer,db.ForeignKey('log.id')) 
	status_id=db.Column(db.Integer,db.ForeignKey('status.id'))            	 #评论的日志id
	user_id=db.Column(db.Integer,db.ForeignKey('users.id'))				 #评论人id
	content=db.Column(db.Text)						 					 #评论内容
	time=db.Column(db.DateTime,default=datetime.now())					 #评论时间
	mod_time=db.Column(db.DateTime)					 					 #最后修改时间

	def getname(self):
		return User.query.filter_by(id=self.user_id).first().nickname

	def gethead(self):
		return User.query.filter_by(id=self.user_id).first().headimg

	def __repr__(self):
		return '<Comment %r>' %self.content

#whooshalchemy.whoosh_index(app,Log)
#if __name__=='__main__':
#	db.drop_all()
#	db.create_all()

whooshalchemy.whoosh_index(app, Status)	

			






	
