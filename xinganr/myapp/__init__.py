#encoding: utf-8
import os
from flask import Flask
from flask import render_template,abort,url_for
from flask.ext.sqlalchemy import SQLAlchemy
from extensions import login_manager
#from config import MAIL_SERVER, MAIL_PORT, MAIL_USERNAME, MAIL_PASSWORD
#from config import basedir,ADMINS
from momentjs import momentjs
from extensions import lazy_gettext
#from extensions import lm,oid
#from myapp import views,models,forms
import flask_sijax




#basedir = os.path.abspath(os.path.dirname(__file__))
path=os.path.join('.',os.path.dirname(__file__),'static/js/sijax')

# UPLOAD_FOLDER = 'statusupimg'

def init_app(app):
    "Initialize app object. Create upload folder if it does not exist."
    if not os.path.isabs(app.config['UPLOAD_FOLDER']):
        folder = os.path.join(os.getcwd(), app.config['UPLOAD_FOLDER'])
        app.config['UPLOAD_FOLDER'] = folder
        print app.config['UPLOAD_FOLDER']
        print 'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh'
    if not os.path.exists(app.config['UPLOAD_FOLDER']):
        os.makedirs(app.config['UPLOAD_FOLDER'])


app = Flask(__name__)

app.config.from_object('config')
init_app(app)
app.config['SIJAX_STATIC_PATH']=path
app.config['SIJAX_JSON_URI']='/static/js/sijax/json2.js'

flask_sijax.Sijax(app)


#app.config.from_object('config')
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost/app'
app.config['SQLALCHEMY_ECHO']=True
app.config['SECRET_KEY']= 'you-will-never-guess'
#app.config['PASSWORD_SECRET'] = r"\x8b N\xba\xe8\x86~\xc94h\xb4N\xa6'\xb9|]g\xc2\xca"
db=SQLAlchemy(app)

#db.create_all()
#db.init_app(app)

login_manager.init_app(app)
login_manager.login_view='user.login'
login_manager.login_message=u'您需要登录后才能访问'
#oid.init_app(app)
#.....all extension,waiting for add....
#lm.login_view='login'
#lm.login_message=lazy_gettext('抱歉！登录后方可浏览该页面.')
#oid=OpenID(app,os.path.join(basedir,'tmp'))
#mail=Mail(app)
#babel=Babel(app)
'''
if not app.debug:
	import logging
	from logging.handlers import SMTPHandler
	credentials=None

	if MAIL_USERNAME or MAIL_PASSWORD:
		credentials=(MAIL_USERNAME,MAIL_PASSWORD)
		mail_handler=SMTPHandler((MAIL_SERVER,MAIL_PORT),'no-reply@'+MAIL_SERVER,ADMINS,'xinganr failure',credentials)
		mail_handler.setLevel(logging.ERROR)
		app.logger.addHandler(mail_handler)

if not app.debug:
	import logging
	from logging.handlers import RotatingFileHandler
	file_handler=RotatingFileHandler('tmp/xinganr.log','a',1*1021*1024,10)
	file_handler.setLevel(logging,INFO)
	file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
	app.logger.addHandler(file_handler)
	app.logger.setLevel(logging.INFO)
	app.logger.info('xinaganr startup')#
'''
#分配处理器
def dispatch_handlers(app):
	d= {}
	@app.errorhandler(403)
	def permission_error(error):
		d['title']=u'您没有权限'
		d['message']=u'您没有权限执行当前的操作, 请登录或检查url是否错误.'
		return render_template('error.html',**d),403
	@app.errorhandler(404)
	def page_not_found(error):
		d['title']=u'页面不存在'
		d['message']=u'呃...貌似您所访问的页面不存在'
		return render_template('error.html',**d),404
	@app.errorhandler(500)
	def page_error(error):
		d['title']=u'页面出错啦'
		d['message']=u'服务器太忙啦,再来吧!'
		return render_template('error.html',**d),500

#注册蓝图
#def dispatch_apps(app):
from myapp.views import siteapp, userapp, diaryapp,pictureapp
app.register_blueprint(siteapp,  url_prefix='/')
app.register_blueprint(diaryapp,  url_prefix='/diary')
app.register_blueprint(userapp)
app.register_blueprint(pictureapp,url_prefix='/picture')


from utils.filters import thumbnail_filter,shorter_url_filter,format_datetime_filter,markdown_filter,pretty_url,current_link_filter,time_passed

app.jinja_env.filters['thumbnail'] = thumbnail_filter
app.jinja_env.filters['shorter_url'] = shorter_url_filter
app.jinja_env.filters['format_datetime'] = format_datetime_filter
app.jinja_env.filters['markdown'] = markdown_filter
app.jinja_env.filters['pretty_url'] = pretty_url
app.jinja_env.filters['current_link'] = current_link_filter
app.jinja_env.filters['timepassed']=time_passed


app.jinja_env.globals['momentjs'] = momentjs

