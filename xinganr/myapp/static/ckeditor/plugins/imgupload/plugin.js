CKEDITOR.plugins.add( 'imgupload', {
    icons: 'imgupload',
    init: function( editor ) {
        //Plugin logic goes here.
        editor.addCommand( 'insertImgupload', {
        	exec: function( editor ) {

        		editor.insertHtml('<img src="/static/headicon/{{g.user.nickname}}" />');
        	}
        });
        editor.ui.addButton( 'imgupload', {
        	label: 'Insert Imgupload',
        	command: 'insertImgupload',
        	toolbar: 'insert'
        });
    }
});