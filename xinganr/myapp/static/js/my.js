// textarea高度自适应
(function($){
	$.fn.adaptiveTextarea = function(options){
		$.fn.adaptiveTextarea.defaults = {
			"maxH":99999,
			"minH":0
		};
		var opts = $.extend({},$.fn.adaptiveTextarea.defaults,options);
		return this.each(function(){
			var $this = $(this);
			var defaultH = opts.minH || $this.height();
				//初始化
			$this.css({
				"overflow":"hidden",
				"resize":"none",
				"height":defaultH + "px"
			});
			$this.off("propertychange input").on("propertychange input",function(){
				this.style.height = defaultH + "px";
				if(opts.maxH >= opts.minH){
					this.style.height = Math.min(this.scrollHeight,opts.maxH) + "px";
				}
			});
		});
	}
})(jQuery);

$(document).ready(function(){
	// logo特效
	$(".fancy_title").lettering();
	// 滑过下拉
	$('.hover').dropdownHover().dropdown();

	// textarea限制字数
	var limitNum = 240;
	var pattern = '还可以输入' + limitNum + '字';
	$('#wordage').html(pattern);
	$('.jishu').keyup(
		function(){
			var remain = $(this).val().length;
			if(remain!=0){
				$("#jishu").removeAttr("disabled");
			}
			if(remain==0||remain>240){
				var result = limitNum - remain;
				pattern = '已经超出' + result + '字';
				$("#jishu").attr("disabled",true);
			}
			else{
				var result = limitNum - remain;
				pattern = '还可以输入' + result + '字';
			}
			$('#wordage').html(pattern);
		}
	);
	// textarea高度
	$(".area").adaptiveTextarea({
		minH:60,
	});
	$(".pub-area").adaptiveTextarea({
		minH:40,
	});
	$(".re-area").adaptiveTextarea({
		minH:25,
	});
	// 插入话题
	$("a.topic").click(function(){
		$(this).parents("form").children("textarea").insertContent("#自定义话题#",-1);
	});
	
	// 分享按钮
	$(".share-set .sina-off").click(function(){
		$(this).toggleClass("sina");
	});
	$(".share-set .douban-off").click(function(){
		$(this).toggleClass("douban");
	});
	$(".share-set .renren-off").click(function(){
		$(this).toggleClass("renren");
	});
	$(".share-set .tencent-off").click(function(){
		$(this).toggleClass("tencent");
	});

	// news-喜欢
   	$("a.heart").click(function(){
   		if($(this).children(".icon-heart-empty").is(":visible")){
   			$(this).children(".icon-heart").show();
   			$(this).children(".icon-heart-empty").hide();
   		}else{
   			$(this).children(".icon-heart-empty").show();
   			$(this).children(".icon-heart").hide();
   		}
   	});
   	// news-tooltip
   	$(".media-heading>a").tooltip();
   	$(".media-footer>a").tooltip({
   		delay:{show:600,hide:100}
   	});

	// 评论框、分享框的显示隐身
	$(".media-footer>a").click(function(){
		$(this).closest(".media-body").children("form").slideToggle("fast");
		$(this).closest(".media-body").children("form").children("textarea").focus();
	});
	$(".ico a.com").click(function(){
		$(this).parents(".media-body").children(".news-comments").slideToggle("fast");
		$(this).parents(".media-body").children(".news-comments").children(".replay").children("textarea").focus();
		$(this).parents(".media-body").children(".share").hide();
	});
	$(".ico a.sha").click(function(){
		$(this).parents(".media-body").children(".share").slideToggle("fast");
		$(this).parents(".media-body").children(".share").children(".replay").children("textarea").focus();
		$(this).parents(".media-body").children(".news-comments").hide();
	});
	// tips名片
	$(".state>.media>.poper").hover(function(){
		$(this).find(".pop").delay(600).animate({opacity: "show", top: "59"}, "fast");
	}, function() {
		$(this).find(".pop").stop(true).animate({opacity: "hide", top: "70"}, "fast");
	});
	$(".news-comments .poper").hover(function(){
		$(this).find(".pop").delay(600).animate({opacity: "show", top: "39"}, "fast");
	}, function() {
		$(this).find(".pop").stop(true).animate({opacity: "hide", top: "50"}, "fast");
	});
	// 关注
	$(".icon-plus-sign-alt").click(function(){
		$(this).fadeOut("fast");
	});
	$(".info .btn").click(function(){
		if($(".attention").is(":visible")){
			$(".attention-suc").show();
   			$(".attention").hide();
		}else{
			$(".attention").show();
   			$(".attention-suc").hide();
		}
	});
	$(".attention-suc").hover(function(){
		if($(this).children(".icon-ok").is(":visible")){
   			$(this).children(".icon-minus").show();
   			$(this).children(".icon-ok").hide();
   		}else{
   			$(this).children(".icon-ok").show();
   			$(this).children(".icon-minus").hide();
   		}
	});
	


	// index-礼物特效
	// $('#gift').grumble(
	// 	{
	// 		text: '快来注册，作一份礼物送给未来的TA！',
	// 		angle: 150,
	// 		distance: 5,
	// 		showAfter: 1000,
	// 		hideAfter: 5000
	// 	}
	// );
	
	//news-右侧固定
	$(window).scroll(function() {
		if($(window).width()>940){
			if($(window).scrollTop()>=228){
				$(".wrapper").addClass("fixedwrapper");
			}else{
				$(".wrapper").removeClass("fixedwrapper");
			} 
		}
  	});

	// 个人主页-相册
	$(".groupItem .photo").hover(function(){
		$(this).children(".img-info-wrapper").slideToggle(200);
	});
	// 个人主页-说明编辑框
	$(".title span a").click(function(){
		$("div.title-content form").slideToggle('fast');
		$("form textarea").focus();
		$("form textarea").val($('span.intro-display').text());
	});

	// // // imgbox  和face的冲突
	// $("a.thumbnail").popImage();
});

//获取当前用户名
$(function() {
	$.getJSON($SCRIPT_ROOT + '/get_current_user', function(data) {
		$('#customer_nickname').text(data.username);
	});
	return false;
});

//创建头像缩略图
$(function() {
	$.getJSON($SCRIPT_ROOT + '/create_headimg_thumbnail', function(data) {
		$('#customer_headimg_thumb').attr("src", function() {
			return data.headimg_thumb
		});
	});
	return false;
});

/*上传新鲜事儿图片*/

$(document).ready(function() {
	$("#status_pic").click(function(event) {
		event.preventDefault();
		$("#statusupimg").click();
	});
});
$(document).ready(function() {
      $('#statusupimg').uploadify({
        'swf': '/static/uploadify.swf',
        'uploader' : '/uploadify',
        'cancelImg': '/static/uploadify-cancel.png',
        'auto': false,
        'multi': true,
        'debug' : false,
	    'queueSizeLimit': 10,
        'removeCompleted': true, // Could set to true, then list completed uploads onComplete
	    'onSelect': function(event, ID, fileObj) {
            $('#upload_link').show();
        },
	    'onAllComplete': function(event, data) {
            $('#upload_link').hide();
        },
        /*订制上传条目显示模板*/
        /*'itemTemplate' : '<div id="${fileID}" class="uploadify-queue-item">\
                    <div class="cancel">\
                        <a href="javascript:$(\'#{statusupimg}\').uploadify(\'cancel\', \'${fileID}\')">X</a>\
                    </div>\
                    <span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>\
                    </div>'*/ 
    });
});



/*//获取新鲜事儿图片
$(function() {
	$.getJSON($SCRIPT_ROOT + '/save_statusupimgs', function(data) {
		$('.thumbnail').attr("src", function() {
			return data.2
		});
	});
	return false;
});*/