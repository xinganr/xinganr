$(function() {
	$.fn.facebox = function() {
		
		//创建表情框
		var faceimg = '';
	    for(i=0;i<60;i++){  //通过循环创建60个表情，可扩展
		 faceimg+='<li><a href="javascript:void(0)"><img src="../static/images/face/'+(i+1)+'.png" face="<emt>'+(i+1)+'</emt>"/></a></li>';
		 };
		$("form .face").append("<div class='facebox'><div class='content'><div>常用表情</div><ul>"+faceimg+"</ul></div></div>");
	     $('.facebox').css("display",'none');//创建完成后先将其隐藏
		//创建表情框结束

		var $facepic = $(".facebox li img");
		//BTN触发事件，显示或隐藏表情层
		$(".face").click(function(){
			if($(this).children(".facebox").is(":hidden")){
				$(this).children(".facebox").show();
			}else{
				$(this).children(".facebox").hide();
			}
		});
		//插入表情
		$facepic.click(function(){
			$('.facebox').hide(360);
			$(this).closest("form").children("textarea").insertContent($(this).attr("face"));
		});

		//当鼠标移开时，隐藏表情层
		 $('.facebox').mouseleave(function(){
			$('.facebox').hide(560);
		});	

  	};  
  
  	// 光标定位插件
	$.fn.extend({  
		insertContent : function(myValue, t) {  
			var $t = $(this)[0];  
			if (document.selection) {  
				this.focus();  
				var sel = document.selection.createRange();  
				sel.text = myValue;  
				this.focus();  
				sel.moveStart('character', -l);  
				var wee = sel.text.length;  
				if (arguments.length == 2) {  
				var l = $t.value.length;  
				sel.moveEnd("character", wee + t);  
				t <= 0 ? sel.moveStart("character", wee - 2 * t	- myValue.length) : sel.moveStart("character", wee - t - myValue.length);  
				sel.select();  
				}  
			} else if ($t.selectionStart || $t.selectionStart == '0') {  
				var startPos = $t.selectionStart;  
				var endPos = $t.selectionEnd;  
				var scrollTop = $t.scrollTop;  
				$t.value = $t.value.substring(0, startPos) + myValue + $t.value.substring(endPos,$t.value.length);  
				this.focus();  
				$t.selectionStart = startPos + myValue.length;  
				$t.selectionEnd = startPos + myValue.length;  
				$t.scrollTop = scrollTop;  
				if (arguments.length == 2) { 
					$t.setSelectionRange(startPos - t,$t.selectionEnd + t);  
					this.focus(); 
				}  
			} else {                              
				this.value += myValue;                              
				this.focus();  
			}  
		}  
	});
 
	//表情解析
	$.fn.extend({
	  replaceface : function(faces){
		  for(i=0;i<60;i++){
			  faces=faces.replace('<emt>'+ (i+1) +'</emt>','<img src="../static/images/face/'+(i+1)+'.png">');
			  }
		   $(this).html(faces);
		   }
	 });
});
