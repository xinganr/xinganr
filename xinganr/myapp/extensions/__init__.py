#encoding: utf-8
# 列出所需的flask扩展
from flask.ext.admin import Admin
#from flask.ext.sqlalchemy import SQLAlchemy 
from flask.ext.login import LoginManager
from flask.ext.openid import OpenID 
from flask.ext.oauth import OAuth
from flask.ext.mail import Mail 
from flask.ext.babel import Babel,lazy_gettext
from flask.ext.cache import Cache
from flask.ext.restless import APIManager
#from flask.ext.Sijax import flask_sijax
from flask.ext.paginate import Pagination

#全部导入
__all__=['login_manager','oid','oau','babel','pag']


#db = SQLAlchemy()
login_manager=LoginManager()
oid=OpenID()
oau=OAuth()
babel=Babel()
pag=Pagination()

